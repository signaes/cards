class RemovesTranslationFromCards < ActiveRecord::Migration[5.0]
  def change
    remove_column :cards, :translation_id
  end
end
