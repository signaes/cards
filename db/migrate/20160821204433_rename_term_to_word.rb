class RenameTermToWord < ActiveRecord::Migration[5.0]
  def change
    rename_column :cards, :term, :word
  end
end
