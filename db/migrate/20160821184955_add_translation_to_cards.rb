class AddTranslationToCards < ActiveRecord::Migration[5.0]
  def change
    add_reference :cards, :translation, foreign_key: true
    add_column :cards, :translation_id
  end
end
