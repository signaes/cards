class Language < ApplicationRecord
  def cards
    Card.where(language_id: id)
  end

  has_many :cards
end
