class Term < ApplicationRecord
  def cards
    Card.where(term_id: id)
  end

  has_many :cards
end
