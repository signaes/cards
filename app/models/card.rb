class Card < ActiveRecord::Base
  def to_s
    word
  end
  
  def translations
    Card.where(term_id: term_id).where.not(id: id)
  end
  
  belongs_to :language
  belongs_to :term
end
