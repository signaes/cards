class HomeController < ApplicationController
  def index
    @version = VERSION
    @card = Card.first
    @word = @card.to_s
    @translations = @card.translations
  end
end
